# moarcatsme

Hubot coffee script to return cat images

## Installation

Run the following command 

    $ npm install hubot-moarcatsme

Then to make sure the dependencies are installed:

    $ npm install

To enable the script, add a `hubot-moarcatsme` entry to the `external-scripts.json`
file (you may need to create this file).

    ["hubot-moarcatsme"]
